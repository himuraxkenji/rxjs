import {fromEvent} from "rxjs";
import {map, tap} from "rxjs/operators";

const texto = document.createElement('div');

texto.innerHTML = `
Consectetur voluptates optio quae ad unde Totam quas accusamus eius soluta saepe Praesentium aliquam vitae facere modi assumenda iure Cumque mollitia dignissimos odio quisquam expedita ad Est molestiae aut quam <br><br>
Consectetur lorem accusantium repellat alias dolore doloribus. Quidem laboriosam neque maiores quod laboriosam? Tempora laudantium maxime quos veritatis quod repudiandae. Quibusdam deleniti fuga numquam necessitatibus impedit. Recusandae inventore sit soluta! <br><br>
Consectetur lorem accusantium repellat alias dolore doloribus. Quidem laboriosam neque maiores quod laboriosam? Tempora laudantium maxime quos veritatis quod repudiandae. Quibusdam deleniti fuga numquam necessitatibus impedit. Recusandae inventore sit soluta! <br><br>
Consectetur lorem accusantium repellat alias dolore doloribus. Quidem laboriosam neque maiores quod laboriosam? Tempora laudantium maxime quos veritatis quod repudiandae. Quibusdam deleniti fuga numquam necessitatibus impedit. Recusandae inventore sit soluta! <br><br>
Consectetur lorem accusantium repellat alias dolore doloribus. Quidem laboriosam neque maiores quod laboriosam? Tempora laudantium maxime quos veritatis quod repudiandae. Quibusdam deleniti fuga numquam necessitatibus impedit. Recusandae inventore sit soluta! <br><br>
Consectetur lorem accusantium repellat alias dolore doloribus. Quidem laboriosam neque maiores quod laboriosam? Tempora laudantium maxime quos veritatis quod repudiandae. Quibusdam deleniti fuga numquam necessitatibus impedit. Recusandae inventore sit soluta! <br><br>
`;


const body = document.querySelector('body');
body.append(texto);

const progressBar = document.createElement('div');
progressBar.setAttribute('class', 'progress-bar');
body.append(progressBar);

// funcion que haga el calculo
const calcularPorcentajeScroll = (event) => {
	const {
		scrollTop,
		scrollHeight,
		clientHeight
	} = event.target.documentElement;

	return (scrollTop / (scrollHeight - clientHeight)) * 100;
}

// Streams

const scroll$ = fromEvent(document, 'scroll');

const progress$ = scroll$.pipe(
	map(calcularPorcentajeScroll),
	tap(console.log),
);


progress$.subscribe(porcentaje => {
	progressBar.style.width = `${porcentaje}%`;
})
